import fs from "fs";
import path from "path";

import fetch from "node-fetch";

(async () => {
  try {
    const octaConfs = JSON.parse(fs.readFileSync("./Octa.json", "utf-8"));

    const myLair = path.resolve("./");

    const res = await fetch(
      path.join(octaConfs.masterNode, "app/dev/IamYourDevelopingChild"),
      {
        method: "POST",
        headers: {
          "content-type": "application/json",
        },
        body: JSON.stringify({
          lair: myLair,
          Octa: octaConfs,
        }),
      }
    );

    const data = await res.json();

    if (res.status !== 200) throw new Error(data.msg);

    console.log(data.msg);

    // console.log(myLair, "myLair");
    // console.log("IntroductionToFather >>> SEND");
  } catch (error) {
    console.error(error);
  }
})();
