import mongoose, { Schema } from "mongoose";

const SCHEMA = new Schema(
  {
    user: Object,
    applications: [Object],
    business: [Object],
    type: String,
  },
  {
    timestamps: true,
  }
);

export default mongoose.model("Dynamics", SCHEMA);
