import fs from "fs";
import path from "path";

export default (req, res) => {
  const root = path.resolve("./Langs");
  const langFiles = fs.readdirSync(root);

  const final = {};

  for (const langFile of langFiles) {
    try {
      const langFilePath = path.join(root, langFile);
      const parsedLangPath = path.parse(langFilePath);
      if (parsedLangPath.ext !== ".json") continue;
      final[parsedLangPath.name] = JSON.parse(fs.readFileSync(langFilePath));
    } catch (error) {
      console.log(error);
      continue;
    }
  }

  res.send(final);
};
