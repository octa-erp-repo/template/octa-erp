export default (req, res) => {
  try {
    res.send(
      `Hi ${req.user.name.toUpperCase(0)} (${
        req.user.id
      }) | this is your Home page, Welcome to OctaniomJS`
    );
  } catch (error) {
    console.log(error);
  }
};
