import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  now: null,
  user: null,
  isLogined: true,
  Loading: true,
  business: [],
  applications: [],
  selectedBusiness: null,
};

const reducers = {
  setNow: (state, arg) => {
    state.now = arg.payload;
  },
  setUser: (state, arg) => {
    octa.info.user = arg.payload;
    state.user = arg.payload;
  },
  setUser: (state, arg) => {
    octa.info.user = arg.payload;
    state.user = arg.payload;
  },
  setApplications: (state, arg) => {
    // octa.info.business = arg.payload;
    state.applications = arg.payload;
  },
  setBusiness: (state, arg) => {
    octa.info.business = arg.payload;
    state.business = arg.payload;
  },
  setSelectedBusiness: (state, arg) => {
    state.selectedBusiness = arg.payload;
  },
  set: (state, arg) => {
    state.ttt = arg.payload;
  },
  startLoad: (state, arg) => {
    state.Loading = true;
  },
  endLoad: (state, arg) => {
    state.Loading = false;
  },
  updateApps: (state, arg) => {
    state.apps = arg.payload;
  },
};

export const context = createSlice({
  name: "store",
  initialState,
  reducers,
});

export const {
  set,
  setUser,
  setBusiness,
  endLoad,
  startLoad,
  updateApps,
  setSelectedBusiness,
  setApplications,
  setNow,
} = context.actions;
export default context.reducer;
