import { configureStore } from "@reduxjs/toolkit";
import store from "./store.js";

export default configureStore({
  reducer: store,
});
