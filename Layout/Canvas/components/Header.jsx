import consts from "../../../consts.js";

export default ({
  pageTitle,
  HeaderHeight,
  sideBarWidth,
  setIsOpenSildebar,
  isOpenSildebar,
  sLang,
}) => {
  const store = Store.useSelector((x) => x);
  return (
    <div
      className="w-100 border-bottom shadow-sm d-flex align-items-center"
      style={{ height: HeaderHeight }}
    >
      <div className="small fw-bold text-muted mx-2 pt-1 d-flex justify-content-between align-items-center">
        <div className="h4">
          {sideBarWidth ? null : (
            <button
              className="btn"
              onClick={() => {
                setIsOpenSildebar(!isOpenSildebar);
              }}
            >
              {isOpenSildebar ? (
                <i className="bi bi-x h3"></i>
              ) : (
                <i className="bi bi-list h3"></i>
              )}
            </button>
          )}
        </div>

        <button
          className="btn btn-sm mx-3"
          onClick={async () => {
            const cookies = new UniversalCookie();
            const cookieName = window?.octa?.meta?.cookieName || "OCTA_ERP";
            const tokenCookie = cookies.get(cookieName);

            if (!store?.user?.selectedClient?.clientId) return;

            const endPoint = `${consts.octaEndPoint}/accounts/log-out/${store.user.selectedClient.clientId}`;

            await octaFetch(endPoint, {
              headers: {
                "api-token": tokenCookie,
              },
            });
            window.location.href = window.location.href;
          }}
        >
          Log out
        </button>
        <Link
          className="h4 text-danger text-decoration-none"
          to={`/p/${sLang}`}
        >
          Octaffice
        </Link>
        {/* {confs?.name} */}

        {/*         
        User name
        {pageTitle ? (
          <small className="opcaity-50">
            {" > "} {pageTitle}
          </small>
        ) : (
          ""
        )} */}
      </div>
    </div>
  );
};
