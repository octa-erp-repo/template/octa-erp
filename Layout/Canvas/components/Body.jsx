export default ({ children, HeaderHeight, sideBarWidth }) => {
  return (
    <div
      className="p-0 border-bottom"
      style={{
        background: "#FBFCFD",
        height: `Calc(100vh - ${HeaderHeight}px)`,
        overflow: "auto",
        width: `Calc(100vw - ${sideBarWidth}px)`,
      }}
    >
      <div className=" mx-1 h-100 p-2">{children}</div>
    </div>
  );
};
