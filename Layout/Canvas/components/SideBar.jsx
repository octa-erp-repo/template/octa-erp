// import QS from "query-string";
import consts from "../../../consts.js";

const OctaLang = ({ id }) => {
  return <>{id}</>;
};

export default ({
  data,
  match,
  HeaderHeight,
  sideBarWidth,
  isOpenSildebar,
  setIsOpenSildebar,
}) => {
  return (
    <>
      {sideBarWidth ? (
        <Body
          className="border-start border-end"
          sideBarWidth={sideBarWidth}
          HeaderHeight={HeaderHeight}
          match={match}
          isOpenSildebar={isOpenSildebar}
          data={data}
        />
      ) : null}

      {!sideBarWidth && isOpenSildebar ? (
        <div
          className="position-fixed"
          id="backkkkk"
          style={{
            width: "100vw",
            height: "100vh",
            zIndex: 99999,
            overflow: "hidden",
            background: "rgba(0, 0, 0, 0.88)",
          }}
          onClick={({ target }) => {
            if (target.id === "backkkkk") setIsOpenSildebar(!isOpenSildebar);
          }}
        >
          <Body
            className="bg-white shadow"
            sideBarWidth={280}
            HeaderHeight={HeaderHeight}
            match={match}
            isOpenSildebar={isOpenSildebar}
            data={data}
          />
        </div>
      ) : null}
    </>
  );
};

const Body = ({
  sideBarWidth,
  HeaderHeight,
  match,
  isOpenSildebar,
  data,
  className,
}) => {
  const store = Store.useSelector((x) => x);
  const selectedLang = match?.params?.lang || "en";
  const rootF = (lang) => {
    return window.octa.uiPath.replace(":LANG", lang);
  };
  const root = rootF(selectedLang);

  const items = [
    {
      url: `${root}/business`,
      title: <OctaLang id="BUSINESS" />,
      icon: "bi bi-building",
    },
    // {
    //   // url: `${root}/users/add`,
    //   title: <OctaLang id="USERS" />,
    //   icon: "bi bi-person",
    //   childs: [
    //     {
    //       url: `${root}/users/list`,
    //       title: <OctaLang id="USERS" />,
    //       icon: "bi bi-person",
    //     },
    //     {
    //       url: `${root}/users/add`,
    //       title: <OctaLang id="ADD_USER" />,
    //       icon: "bi bi-person-add",
    //     },
    //   ],
    // },
    {
      title: <OctaLang id="APPLICATION" />,
      icon: "bi bi-circle",
      childs: [
        {
          url: `${root}/applications/managment`,
          title: <OctaLang id="APP-MANAGE" />,
          icon: "bi bi-wrench-adjustable-circle",
        },
        {
          url: `${root}/applications/runner`,
          title: "runner",
          icon: "bi bi-cpu",
          type: "user",
        },
      ],
    },
    {
      url: `${root}/accounts`,
      title: <OctaLang id="ACCOUNTS" />,
      icon: "bi bi-building",
    },
    // {
    //   url: `${root}/access`,
    //   title: <OctaLang id="ACCESS" />,
    //   icon: "bi bi-key",
    // },
    // {
    //   url: `${root}/api-tokens`,
    //   title: <OctaLang id="API_TOKENS" />,
    //   icon: "bi bi-lock",
    // },
    // {
    //   url: `${root}/setting`,
    //   title: <OctaLang id="SETTING" />,
    //   icon: "bi bi-gear",
    // },
    {
      url: `${root}/db`,
      title: <OctaLang id="DB" />,
      icon: "bi bi-boxes",
    },
    {
      title: "developers",
      icon: "bi bi-code",
      childs: [
        {
          url: `${root}/applications/new`,
          title: <OctaLang id="APP_NEW" />,
          icon: "bi bi-plus-circle",
        },
        {
          url: `${root}/applications/new-db-collection`,
          title: <OctaLang id="NEW_COLLECTION" />,
          icon: "bi bi-database",
        },
        {
          url: `${root}/documentation`,
          title: "documentation",
          icon: "bi bi-file",
        },
      ],
    },
    {
      url: `${root}/help`,
      title: "help",
      icon: "bi bi-question-circle",
    },
    {
      url: `${root}/suport`,
      title: "suport",
      icon: "bi bi-headset",
    },
  ];

  const devItems =
    window.octa.stage === "DEVELOPMENT"
      ? [
          {
            url: `${root}/dev/redux`,
            title: "redux",
            icon: "bi bi-sd-card",
          },

          {
            url: `${root}/dev/confs`,
            title: "window.octa",
            icon: "bi bi-info-circle",
          },
          {
            url: `${root}/dev/langs`,
            title: "langs",
            icon: "bi bi-translate",
          },
          {
            url: `#`,
            title: "apis",
            icon: "bi bi-ethernet",
          },
          {
            url: `${root}/dev/build`,
            title: "build",
            icon: "bi bi-bank",
          },
          {
            url: `#`,
            title: "config",
            icon: "bi bi-braces",
          },
        ]
      : [];

  return (
    <>
      <div
        className={`px-0 ${className}`}
        style={{
          height: `Calc(100vh - ${HeaderHeight || 0}px)`,
          overflow: "auto",
          width: sideBarWidth,
        }}
      >
        <div
          className="d-flex py-2 text-center justify-content-center flex-wrap border-bottom"
          style={{
            background: "#FBFCFD",
          }}
        >
          <i className="bi bi-person-circle w-100"></i>
          <div className="small fw-bold w-100">
            {store?.user?.firstName} |
            <span className="small mx-1 text-capitalize">
              {store?.selectedBusiness?.name || "-"}
            </span>
          </div>
          <div className="small w-100">{store?.user?.email}</div>
          {/* <div className="w-100 px-1">
            <div className="small w-100 d-block text-center text-white bg-dark opacity-75">
              {store?.selectedBusiness?.name || "-"}
            </div>
          </div> */}
        </div>
        <div className="border-bottom- m-0 p-0" style={{ overflowY: "auto" }}>
          {items.map((x, i) => {
            if (x.type && x.type !== consts.type) return null;
            if (x.childs) {
              let isChildSelected = x.childs?.some((y) => checkPage(y?.url));

              return (
                <div
                  className="accordion accordion-flush border-bottom-"
                  id={"SidbatAccordion"}
                >
                  <div className="accordion-item">
                    <div
                      className="accordion-header"
                      id={"flush-headingOne" + i}
                    >
                      <div className="px-2">
                        <button
                          className={`accordion-button collapsed px-3 py-2 mt-1  ps-1 rounded-3 ${
                            isChildSelected
                              ? "collapsed alert-primary"
                              : " border-0"
                          } `}
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target={"#flush-collapseOne" + i}
                          aria-expanded={`${
                            isChildSelected ? "true" : "false"
                          }`}
                          aria-controls={"flush-collapseOne" + i}
                        >
                          {x.icon ? (
                            <i className={`${x.icon} mx-2`}></i>
                          ) : (
                            " O "
                          )}
                          {x.title}
                        </button>
                      </div>
                    </div>
                    <div className="px-2">
                      <div
                        id={"flush-collapseOne" + i}
                        className={`accordion-collapse collapse ${
                          isChildSelected ? "show" : ""
                        }`}
                        aria-labelledby={"flush-headingOne" + i}
                        data-bs-parent={"#SidbatAccordion"}
                      >
                        <div className="accordion-body p-1 border bg-light rounded-3 mt-1">
                          {x?.childs?.map((y, j) => {
                            if (y.type && y.type !== consts.type) return null;

                            return (
                              <Link
                                key={j}
                                to={y.url}
                                className={`btn btn-sm small px-0 py-2 ${
                                  items.length - 1 !== j ? "border-bottom-" : ""
                                } d-flex btn rounded-3- justify-content-start  ${
                                  checkPage(y.url)
                                    ? "alert-primary"
                                    : "bg-light"
                                }`}
                              >
                                <i className={`bi bi-dot`}></i>{" "}
                                {y.icon ? (
                                  <i className={`${y.icon} mx-1`}></i>
                                ) : (
                                  " O "
                                )}
                                {y.title}
                              </Link>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            }
            return (
              <div className="px-2">
                <Link
                  key={i}
                  to={x.url}
                  className={`btn ps-1 mt-1 ${
                    items.length - 1 !== i ? "border-bottom-" : ""
                  } d-flex btn justify-content-start  ${
                    checkPage(x.url) ? "alert-primary" : ""
                  }`}
                >
                  {x.icon ? <i className={`${x.icon} mx-2`}></i> : " O "}
                  {x.title}
                </Link>
              </div>
            );
          })}

          {devItems.length ? (
            <>
              <div className="px-1">
                <div className="small my-2 d-block text-center text-white bg-dark opacity-75">
                  developmant pages
                </div>
              </div>
              {devItems.map((x, i) => {
                return (
                  <div className="px-2">
                    <Link
                      key={i}
                      to={x.url}
                      className={`btn ${
                        devItems.length - 1 !== i ? "border-bottom- mb-1" : ""
                      } btn d-flex btn-sm small justify-content-start  ${
                        checkPage(x.url) ? "alert-primary" : "text-muted"
                      }`}
                    >
                      {x.icon ? <i className={`${x.icon} mx-2`}></i> : " O "}
                      {x.title}
                    </Link>
                  </div>
                );
              })}
            </>
          ) : null}
        </div>
      </div>
    </>
  );
};

const checkPage = (pageUrl) => {
  const winUrl = window.location.pathname;
  if (winUrl === pageUrl) return true;
  return false;
};
