export default ({ ...a }) => {
  const store = Store.useSelector((x) => x);
  return (
    <div>
      <div>
        <div className="h1">Redux page</div>
      </div>
      <br />
      <br />
      <br />
      redux store
      <pre>{JSON.stringify(store, null, 2)}</pre>
    </div>
  );
};
